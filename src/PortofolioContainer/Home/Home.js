import React from 'react';
import Profile from './Profile/Profile';
import Footer from './Footer/Footer';
import './Home.css';
import AboutMe from '../AboutMe/AboutMe';

export default function Home() {
    return (
        <div className='home-container'>
            <Profile/>
            <Footer/>
            <AboutMe/>
            {/* jika anda menukar footer ke atas kamu akan mendapatkan hasil yang berbeda */}
            
        </div>
    );
}
