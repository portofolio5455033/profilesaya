import React from "react";
// import Typical from "react-typical";
import "./Profile.css";

export default function Profile() {
    return (
        <div className='profile-container'>
            <div className='profile-parent'>
                <div className='profile-details'>
                    <div className='colz'>
                        <div className='colz-icon'>
                            <a href='https://www.facebook.com/noval.streal.7'>
                                <i className='fa fa-facebook-square'></i>
                            </a>
                            <a href='https://myaccount.google.com/profile'>
                                <i className='fa fa-google-plus-square'></i>
                            </a>
                            <a href='https://www.instagram.com/novaldi53/'>
                                <i className='fa fa-instagram'></i>
                            </a>
                            <a href='https://www.youtube.com/@kenzo2039'>
                                <i className='fa fa-youtube-square'></i>
                            </a>
                            <a href='https://twitter.com/i/flow/login'>
                                <i className='fa fa-twitter'></i>
                            </a>
                        </div>
                    </div>
                
                    <div className='Profile-details-name'>
                        <span className='primary-text'>
                            {" "}
                            Hay, Saya <span className='highlighted-text'>Novaldi</span>
                        </span>
                    </div>
                    <div className='profile-details-role'>
                        <span className='primary-text'>
                            {" "}
                            <h1>
                                {/* {" "}
                                <Typical
                                    loop={Infinity}
                                    wrapper="p"
                                    steps={[
                                        "Novaldi Dev",
                                        1000,
                                        "Python Developer",
                                        1000,
                                        "HTML Dev",
                                        1000,
                                        "Cross Platfrom v",
                                        1000,
                                        "React/React Native Dev",
                                        1000,
                                    ]}
                                /> */}
                            </h1>
                            <span className='profile-role-tagline'>
                                Bakat membangun aplikasi dengan front and operasi.
                            </span>
                        </span>
                    </div>
                    <div className='profile-options'>
                        <a href='#tentang'>
                        <button className='btn primary-btn'>
                            {""}
                            Saya{}
                        </button>
                        </a>
                        <a href='Resume.pdf' download='CV Noval.pdf'>
                            <button className='btn highlighted-btn'>Dapatkan CV</button>
                        </a>
                    </div>
                </div>
                <div className='profile-picture'>
                    <div className='profile-picture-background'></div>
                </div>
            </div>
        </div>
    );
}
